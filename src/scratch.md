# SillyGame Thoughts

## Objects

1. Game
2. Player
3. Cards

## Main Process

1. Create __Players__ from the list supplied.
   1. _Name_
   2. _Cards_
      1. create __Cards__ from the stings supplied
   3. Bet

2. For each player
   1. check whether they have a __Straight Flush__
      * return **true** plus the highest card in the Straight Flush
      * return **false**  

   1. ... if not, check whether they have a __Four of Kind__
      * return **true** plus the highest card in the Four of a Kind
      * return **false**  

   1. ... if not, check whether they have a __Full House__
      * return **true** plus the highest card in the set of three in the Full House
      * return **false**

   1. check whether they have a __Flush__
      * return **true** plus the list of card ranks
      * return **false**

   1. .. if not, check whether they have a __Three of  Kind__
      * return **true** plus the highest rank card in the triple
      * return **false**

   1. ... if not, check whether they have a __Two Pair__
      * return **true** plus rank of each pair and rank of the kicker
      * return **false**

   1. ... if not, check whether they have a __Pair__
      * return **true** plus the rank of the pair and the rank of all three kickers
      * return **false**

   1. .. if not, check what their __Highest Value__ card is.
      * return **true**? and highest value card

3.