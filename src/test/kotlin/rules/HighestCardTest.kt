package rules

import Card
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class HighestCardTest {

    // 2,2,3 beats 2,2,2

    @Test
    fun `2,2,3 beats 2,2,2`() {
        val handA = listOf(
            Card(value = 2),
            Card(value = 2),
            Card(value = 3)
        )

        val handB = listOf(
            Card(value = 2),
            Card(value = 2),
            Card(value = 2)
        )

        val winner = HighestCard().findWinningHand(handA, handB)

        assertThat(winner).`as`("A then B").containsOnly(handA)

        val winner2 = HighestCard().findWinningHand(handB, handA)

        assertThat(winner2).`as`("B then A").containsOnly(handA)
    }

    @Test
    fun `3, 3 beats 3, 2`() {
        val handA = listOf(
            Card(value = 3),
            Card(value = 3)
        )

        val handB = listOf(
            Card(value = 3),
            Card(value = 2)
        )

        val winner = HighestCard().findWinningHand(handA, handB)

        assertThat(winner).`as`("A then B").containsOnly(handA)

        val winner2 = HighestCard().findWinningHand(handB, handA)

        assertThat(winner2).`as`("B then A").containsOnly(handA)
    }



    @Test
    fun `2,2,2 draws with 2,2,2`() {
        val handA = listOf(
            Card(value = 2),
            Card(value = 2),
            Card(value = 2)
        )

        val handB = listOf(
            Card(value = 2),
            Card(value = 2),
            Card(value = 2)
        )

        val winners = HighestCard().findWinningHand(handA, handB)

        assertThat(winners).containsOnly(handA, handB)
    }

}

