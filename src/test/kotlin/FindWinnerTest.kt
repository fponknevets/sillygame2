import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class FindWinnerTest {
    @Test
    fun `should find winner`() {
        val stevenloser = Player("Steven", listOf("2S", "3S", "4S", "5S", "6S"))
        val carolynwinner = Player("Carolyn", listOf("7S", "8S", "9S", "TS", "JS"))

        val myGame = Game()
        val winner = myGame.findWinners(stevenloser, carolynwinner)
        assertThat(winner).isEqualTo(carolynwinner)

        // Given multiple players

        // and each player's hand that they are playing of up to 5 cards

        // then return the highest scoring player?

        // TODO in future
        // * make sure the player is actually using their 2 cards + cards from the table (and not random cards
        // * bets
    }
}