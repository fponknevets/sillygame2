package rules

import Card

class HighestCard {

    fun findWinningHand(hand1: List<Card>, hand2: List<Card>): List<List<Card>> {
        // sort the two hands
        val hand1Sorted = hand1.sortedBy { it.value }
        val hand2Sorted = hand2.sortedBy { it.value }
        // iterate through both hands
        (hand1Sorted.indices).forEach { i ->
            // compare the first value in each sorted hand
            // if one is larger than the other return that hand
            if (hand1Sorted[i].value > hand2Sorted[i].value) {
                return listOf(hand1)
            } else if (hand2Sorted[i].value > hand1Sorted[i].value) {
                return listOf(hand2)
            }
            // if both are equal go to the next iteration
            // if we get to the last card in each hand then return both hands
        }
        return listOf(hand1, hand2)
    }

}